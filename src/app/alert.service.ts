import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private toastr: ToastrService) { }
  error:boolean;
  msg:string;
  show:boolean;

  showAlert(message: string ,error:boolean ) {
    this.msg = message;
    this.error = error;
    if(this.error){
      this.toastr.error('Error!', this.msg);
    }else{
      this.toastr.success('Success!', this.msg);
    }
    // this.show = true
  }
  hideAlert( ) {
    this.show = false
  }
}
