import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  autherName:string;
  constructor(private router: Router, private authService: AuthService) {
    console.log(JSON.parse(localStorage.getItem('user')).user.name)
    this.autherName = JSON.parse(localStorage.getItem('user')).user.name
    // if (!localStorage.getItem('user')) {
    //   this.router.navigate(['/login']);
    // }
  }

  ngOnInit(): void {
  }

  loadBlogsTable():void{
    console.log('Load Blogs');
  }

  logout(): void {

    // console.log(loginform.value);
    this.authService.logout().pipe(first()).subscribe({
      next: (response) => {
        // this.error = false;
        // console.log(JSON.parse(localStorage.getItem('user')?"true":"false"));
        // this.alertService.success('Registration successful', { keepAfterRouteChange: true });
        this.router.navigate(['/login']);
      },
      error: error => {
        // this.error = true;
        // this.errorMsg = error.error.message;
        console.log(error.error.message);

        // this.alertService.error(error);
        // this.loading = false;
      }

    });
  }

}
