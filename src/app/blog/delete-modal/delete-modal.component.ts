import { MyBlogsComponent } from './../my-blogs/my-blogs.component';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Inject } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { BlogService } from '../blog.service';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/alert.service';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.css']
})
export class DeleteModalComponent implements OnInit {

  constructor(private alertService :AlertService,private router: Router, private blogService: BlogService, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<DeleteModalComponent>) {
  this.del_type = this.data.del_type;
  }
  del_type:any;
  ngOnInit(): void {
    console.log(this.data.data)
  }
  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }

  @Input() blog_id: string;
  mySubscription: any;
  actionFunction() {
    let blog_id = this.data.data;

    if(this.del_type == 'category'){
      this.blogService.delCategory(blog_id).pipe(first()).subscribe({
        next: (response) => {
          // this.error = false;
          console.log(response);
          this.closeModal();
          let currentUrl = this.router.url;
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate([currentUrl]);
          this.alertService.showAlert('Category Delete Successfull' , false);
        },
        error: error => {

          console.log(error.error.message);
          this.alertService.showAlert(error.error.message , true);

        }

      });
    }else{
      this.blogService.inactiveBlog(blog_id).pipe(first()).subscribe({
        next: (response) => {
          // this.error = false;
          console.log(response);
          this.closeModal();
          let currentUrl = this.router.url;
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate([currentUrl]);
          this.alertService.showAlert('Blog Inactive Successfull' , false);
        },
        error: error => {

          console.log(error.error.message);
          this.alertService.showAlert(error.error.message , true);

        }

      });
    }


  }

  closeModal() {
    this.dialogRef.close();
  }

}
