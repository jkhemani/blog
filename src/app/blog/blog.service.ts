import { AuthService } from 'src/app/auth/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { apiUrl } from 'src/globals';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  apiURL: string = apiUrl();

  constructor(private httpClient: HttpClient , private authService :AuthService) { }

  public getCategories(parent_id = 0 ) {

    return this.httpClient.post<string>(`${this.apiURL}categories`, {'parent_id':parent_id ,'all':""}, this.authService._header())
      .pipe(map(response => {
        return response;
        // remove user from local storage and set current user to null
        // localStorage.removeItem('user');
      }));
  }
  public getUsers(user_id = 0) {

    return this.httpClient.post<string>(`${this.apiURL}users`, {'user_id':user_id,'except':user_id}, this.authService._header())
      .pipe(map(response => {
        return response;
        // remove user from local storage and set current user to null
        // localStorage.removeItem('user');
      }));
  }
  public storeCategories(post) {

    return this.httpClient.post<string>(`${this.apiURL}categories/store`, {'parent_id':post[0],'category':post[1]}, this.authService._header())
      .pipe(map(response => {
        return response;
        // remove user from local storage and set current user to null
        // localStorage.removeItem('user');
      }));
  }
  public updateCategories(updateCat) {

    return this.httpClient.post<string>(`${this.apiURL}categories/update`, updateCat, this.authService._header())
      .pipe(map(response => {
        return response;
        // remove user from local storage and set current user to null
        // localStorage.removeItem('user');
      }));
  }

  public updateBlog(blogPost){
    console.log(blogPost);
    const headers = this.authService._header()
    return this.httpClient.post<string>(`${this.apiURL}blogs/update`, blogPost, headers)
      .pipe(map(response => {
        return response;
      }));

  }
  public storeComment(comment , blog_id , user_id) {

    return this.httpClient.post<string>(`${this.apiURL}comments/store`, {'comment':comment['comment'],'user_id':user_id , 'blog_id':blog_id}, this.authService._header())
      .pipe(map(response => {
        return response;
        // remove user from local storage and set current user to null
        // localStorage.removeItem('user');
      }));
  }
  public storeLike( blog_id , user_id) {

    return this.httpClient.post<string>(`${this.apiURL}likes/store`, {'user_id':user_id , 'blog_id':blog_id}, this.authService._header())
      .pipe(map(response => {
        return response;
        // remove user from local storage and set current user to null
        // localStorage.removeItem('user');
      }));
  }
  public storeBlog(blogPost ) {
    console.log(blogPost);
    const headers = this.authService._header()
    return this.httpClient.post<string>(`${this.apiURL}blogs/store`, blogPost, headers)
      .pipe(map(response => {
        return response;
      }));
  }
  public publishBlog(blog_id) {


    return this.httpClient.post<string>(`${this.apiURL}blogs/publish`, {'blog_id':blog_id}, this.authService._header())
      .pipe(map(response => {
        return response;
      }));
  }
  public inactiveBlog(blog_id) {


    return this.httpClient.post<string>(`${this.apiURL}blogs/inactive`, {'blog_id':blog_id}, this.authService._header())
      .pipe(map(response => {
        return response;
      }));
  }
  public delCategory(blog_id) {


    return this.httpClient.post<string>(`${this.apiURL}categories/delete`, {'category_id':blog_id}, this.authService._header())
      .pipe(map(response => {
        return response;
      }));
  }
}
