import { BlogService } from './../blog.service';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/alert.service';

@Component({
  selector: 'app-categories-create',
  templateUrl: './categories-create.component.html',
  styleUrls: ['./categories-create.component.css']
})
export class CategoriesCreateComponent implements OnInit {
  public categories = {};
  public categories_typed: string[] = [];

  constructor(private alertService :AlertService,private blogService: BlogService) { }

  ngOnInit(): void {
  }
  get cat_len() :number{
    return Object.keys(this.categories).length;
  }

  categorySubmit(): void{
    this.categories[this.categories_typed.length-1] =[];
    this.categories[this.categories_typed.length-1][1] = this.categories_typed[this.categories_typed.length-1];

    this.categories[this.categories_typed.length-1][0] = this.categories[this.categories_typed.length-2] ? this.categories[this.categories_typed.length-2][0]:0;
    // console.log(registerForm.value);
    this.blogService.storeCategories(this.categories[this.categories_typed.length-1]).pipe(first()).subscribe({
      next: (response) => {

        this.categories[this.categories_typed.length-1][0] = response['category']['id'];
        console.log(JSON.stringify(response));
        this.alertService.showAlert('Category Created Successfull' , false);

      },
      error: error => {
        this.alertService.showAlert(error.error.message , true);

      }

    });
    // let idx = Object.keys(this.categories).length;
    // this.categories[idx] =
  }

}
