import { BlogService } from './../blog.service';
import { Component, OnInit } from '@angular/core';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { first } from 'rxjs/operators';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { AlertService } from 'src/app/alert.service';
import { apiUrl, mediaUrl } from 'src/globals';
import { AuthService } from 'src/app/auth/auth.service';
import { ActivatedRoute } from '@angular/router';
// import  * as $ from "jquery";
declare var $: any;


@Component({
  selector: 'app-edit-blog',
  templateUrl: './edit-blog.component.html',
  styleUrls: ['./edit-blog.component.css']
})
export class EditBlogComponent implements OnInit {

  public Editor = ClassicEditor;
  public categories = {};
  public authers: any;
  error: boolean;
  errorMsg: string;
  category_id: number;
  blogForm: FormGroup;
  category: string;
  chng_category = false;
  chng_thumbnail = false;
  blog: any[];


  constructor(private route: ActivatedRoute, private alertService: AlertService, private router: Router, private blogService: BlogService, private fb: FormBuilder, private httpClient: HttpClient, private authService: AuthService) {
    console.log(this.Editor);
    this.getCategories(0, 0);
    this.route.params.subscribe(event => {

      this.blog_id = event.blog_id;
    });



  }

  ngOnInit(): void {
    this.blogForm = this.fb.group({
      blog_id: ['', Validators.required],
      title: ['', Validators.required],
      body: ['', [Validators.required]],
      visibility: ['',],
      user_id: ['', [Validators.required]],
      status: ['', [Validators.required]],
      authers: [''],
      category_id: ['', [Validators.required]],
      thumbnail: ['', [RxwebValidators.extension({ extensions: ["jpeg", "png", "jpg"] })]],
    });


    this.getUsers();
    this.getBlogData();
  }
  getBlogData(): void {
    const user_id = JSON.parse(localStorage.getItem('user'))['user'].id;

    console.log(user_id)
    this.httpClient.post<string>(apiUrl() + 'blogs',
      { 'user_id': user_id, 'blog_id': this.blog_id, 'edit': 'edit' }, this.authService._header())
      .subscribe(data => {
        console.log(data);
        this.blog = data['blogs'];
        let authers = []
        for (let i = 0; i < this.blog[0].private.length; i++) {
          authers.push(String(this.blog[0].private[i]['user_id']));
        }
        console.log(authers);
        this.blogForm.patchValue({
          'blog_id': this.blog[0].id,
          'title': this.blog[0].title,
          'body': this.blog[0].body,
          'visibility': this.blog[0].visibility,
          'authers': authers
        })

        this.category = this.blog[0].category[0]['blog_category'];
        let _media = mediaUrl();
        this.imageURL = _media + 'blog_thumbnails/' + this.blog[0].thumbnail;
        $('.selectpicker').selectpicker();

      });
  }
  get blogFormControl() {
    return this.blogForm.controls;
  }

  getCategories(idx, parent_id): void {
    if (idx < 5) {
      for (let i = idx; i <= 5; i++) {
        delete this.categories[i];
      }
    }
    console.log(parent_id)
    if (parent_id != 0) {
      this.category_id = parent_id;
      this.blogForm.patchValue({
        category_id: this.category_id,
      });
    }
    this.blogService.getCategories(parent_id).pipe(first()).subscribe({
      next: (response) => {
        if (response['categories'].length > 0) {
          this.categories[idx] = response['categories'];
          console.log(this.categories);
        }
      },
      error: error => {
        console.log(error)
      }

    });

  }
  getUsers(): void {
    let user_id = JSON.parse(localStorage.getItem('user'))['user'].id

    this.blogService.getUsers(user_id).pipe(first()).subscribe({
      next: (response) => {
        if (response['users'].length > 0) {
          this.authers = response['users'];
          console.log(this.authers);
          $.fn.selectpicker.Constructor.BootstrapVersion = '4';

        }
      },
      error: error => {
        console.log(error)
      }

    });

  }
  ngAfterViewInit() {


    // document.getElementById('preloader').classList.add('hide');

  }
  filedata: any;
  blog_id: any;
  imageURL: string;
  /* File onchange event */
  fileEvent(e) {
    this.filedata = e.target.files[0];
    console.log(this.blogFormControl.thumbnail.value)
    const reader = new FileReader();
    reader.onload = () => {
      this.imageURL = reader.result as string;
      this.chng_thumbnail = true;
    }
    reader.readAsDataURL(this.filedata)
  }
  status = 1

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  onSubmit(): void {

    this.blogForm.patchValue({
      category_id: this.chng_category ? this.category_id : this.blog[0].category_id,
      status: this.status,
      user_id: JSON.parse(localStorage.getItem('user'))['user'].id

    })
    if (this.blogForm.invalid) {
      console.log(this.blogForm);
      this.validateAllFormFields(this.blogForm);
      return;
    }


    // console.log(this.filedata);
    // console.log(this.blogForm.value);
    var myFormData = new FormData();
    if (this.chng_thumbnail) {
      myFormData.append('image', this.filedata);
    }
    myFormData.append('image_name', this.blog[0].thumbnail);
    myFormData.append('chg_image', String(this.chng_thumbnail));
    myFormData.append('blog_id', this.blogForm.value['blog_id']);
    myFormData.append('title', this.blogForm.value['title']);
    myFormData.append('body', this.blogForm.value['body']);
    myFormData.append('category_id', this.blogForm.value['category_id']);
    myFormData.append('status', this.blogForm.value['status']);
    myFormData.append('visibility', this.blogForm.value['visibility']);
    myFormData.append('user_id', this.blogForm.value['user_id']);
    myFormData.append('authers', this.blogForm.value['authers']);
    console.log(myFormData);
    // myFormData.append();
    this.blogService.updateBlog(myFormData).pipe(first()).subscribe({
      next: (response) => {
        this.error = false;
        console.log(response);
        this.status = 2;
        this.blog_id = response['blog_id'];
        this.alertService.showAlert('Blog Updated And Drafted Successfully', false);
        // console.log(JSON.parse(localStorage.getItem('user')?"true":"false"));
        // this.alertService.success('Registration successful', { keepAfterRouteChange: true });
        // this.router.navigate(['/blogs/']);
      },
      error: error => {
        this.error = true;
        this.errorMsg = error.error.message;
        if (this.errorMsg == "Unauthenticated.") {
          this.errorMsg = "Session Expired";
          this.router.navigate(['/login']);
        }
        console.log(error.error.message);
        this.alertService.showAlert(error.error.message, true);
        // this.alertService.error(error);
        // this.loading = false;
      }

    });
    if (this.status == 2) {
      console.log('Drafted ready to publish')
      this.blogService.publishBlog(this.blogForm.value['blog_id']).pipe(first()).subscribe({
        next: (response) => {
          this.error = false;
          console.log(response);
          // this.status = 2;
          // this.blog_id = response['blog_id'];
          // console.log(JSON.parse(localStorage.getItem('user')?"true":"false"));
          // this.alertService.success('Registration successful', { keepAfterRouteChange: true });
          this.router.navigate(['/myblogs/']);
          this.alertService.showAlert('Blog Updated And Published Successfully', false);
        },
        error: error => {
          this.error = true;
          this.errorMsg = error.error.message;
          if (this.errorMsg == "Unauthenticated.") {
            this.errorMsg = "Session Expired";
            this.router.navigate(['/login']);
          }
          console.log(error.error.message);
          this.alertService.showAlert(error.error.message, true);
          // this.alertService.error(error);
          // this.loading = false;
        }

      });

    }
  }

}
