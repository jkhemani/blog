import { BlogService } from './../blog.service';
import { Component, OnInit } from '@angular/core';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { first } from 'rxjs/operators';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { AlertService } from 'src/app/alert.service';
// import  * as $ from "jquery";
declare var $: any;
// import $ from 'jquery';
import { IMultiSelectOption,IMultiSelectTexts, IMultiSelectSettings } from 'ngx-bootstrap-multiselect';

@Component({
  selector: 'app-create-blog',
  templateUrl: './create-blog.component.html',
  styleUrls: ['./create-blog.component.css']
})
export class CreateBlogComponent implements OnInit {
  public Editor = ClassicEditor;
  public categories = {};
  public authers = [];
  error: boolean;
  errorMsg: string;
  category_id: number;
  blogForm: FormGroup;
  optionsModel: number[];
  myOptions: IMultiSelectOption[];
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 3,
    displayAllSelectedText: true
  };
  // Text configuration
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    searchEmptyResult: 'Nothing found...',
    searchNoRenderText: 'Type in search box to see results...',
    defaultTitle: 'Select',
    allSelected: 'All selected',
  };

  constructor(private alertService: AlertService, private router: Router, private blogService: BlogService, private fb: FormBuilder,) {
    console.log(this.Editor);
    this.getCategories(0, 0);




  }

  ngOnInit(): void {
    this.blogForm = this.fb.group({
      title: ['', Validators.required],
      body: ['', [Validators.required]],
      visibility: ['1', [Validators.required],],
      user_id: ['', [Validators.required]],
      status: ['', [Validators.required]],
      authers: [],
      category_id: ['', [Validators.required]],
      thumbnail: ['', [Validators.required, RxwebValidators.extension({ extensions: ["jpeg", "png", "jpg"] })]],
    }
    );
    this.getUsers();





  }
  get blogFormControl() {
    return this.blogForm.controls;
  }

  getCategories(idx, parent_id): void {
    if (idx < 5) {
      for (let i = idx; i <= 5; i++) {
        delete this.categories[i];
      }
    }
    console.log(parent_id)
    if (parent_id != 0) {
      this.category_id = parent_id;
      this.blogForm.patchValue({
        category_id: this.category_id,
      });
    }
    this.blogService.getCategories(parent_id).pipe(first()).subscribe({
      next: (response) => {
        if (response['categories'].length > 0) {
          this.categories[idx] = response['categories'];
          console.log(this.categories);
        }
      },
      error: error => {
        console.log(error)
      }

    });

  }
  getUsers(): void {
    let user_id = JSON.parse(localStorage.getItem('user'))['user'].id

    this.blogService.getUsers(user_id).pipe(first()).subscribe({
      next: (response) => {
        if (response['users'].length > 0) {
          for (let i = 0; i < response['users'].length; i++) {
            this.authers.push({
              id: response['users'][i]['id'],
              name: response['users'][i]['name']
            })
          }
          this.myOptions = this.authers;
          this.authers = response['users'];
          console.log(this.authers);
          $.fn.selectpicker.Constructor.BootstrapVersion = '4';

        }
      },
      error: error => {
        console.log(error)
      }

    });

  }
  ngAfterContentInit() {



    // console.log("hi")


    // document.getElementById('preloader').classList.add('hide');
    // $('.selectpicker').selectpicker();
  }
  filedata: any;
  blog_id: any;
  imageURL: string;
  /* File onchange event */
  fileEvent(e) {
    this.filedata = e.target.files[0];
    console.log(this.blogFormControl.thumbnail.value)
    const reader = new FileReader();
    reader.onload = () => {
      this.imageURL = reader.result as string;
    }
    reader.readAsDataURL(this.filedata)
  }
  status = 1
  // blogForm = new FormGroup({
  //   category_id: new FormControl(),
  //   title: new FormControl(),
  //   body: new FormControl(),
  //   thumbnail: new FormControl(),
  //   status: new FormControl(),
  //   visibility: new FormControl(),
  //   user_id: new FormControl()
  // })
  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  onSubmit(): void {
    if (this.status == 1) {
      this.blogForm.patchValue({
        category_id: this.category_id,
        status: this.status,
        user_id: JSON.parse(localStorage.getItem('user'))['user'].id

      })
      if (this.blogForm.invalid) {
        console.log(this.blogForm);
        this.validateAllFormFields(this.blogForm);
        return;
      }


      console.log(this.optionsModel);
      console.log(this.blogForm.value);
      var myFormData = new FormData();
      myFormData.append('image', this.filedata);
      myFormData.append('title', this.blogForm.value['title']);
      myFormData.append('body', this.blogForm.value['body']);
      myFormData.append('category_id', this.blogForm.value['category_id']);
      myFormData.append('status', this.blogForm.value['status']);
      myFormData.append('visibility', this.blogForm.value['visibility']);
      myFormData.append('user_id', this.blogForm.value['user_id']);
      myFormData.append('authers', this.blogForm.value['authers']);
      console.log(myFormData);
      // myFormData.append();
      this.blogService.storeBlog(myFormData).pipe(first()).subscribe({
        next: (response) => {
          this.error = false;
          console.log(response);
          this.status = 2;
          this.blog_id = response['blog_id'];
          this.alertService.showAlert('Blog Drafted Successfully', false);
          // console.log(JSON.parse(localStorage.getItem('user')?"true":"false"));
          // this.alertService.success('Registration successful', { keepAfterRouteChange: true });
          // this.router.navigate(['/blogs/']);
        },
        error: error => {
          this.error = true;
          this.errorMsg = error.error.message;
          if (this.errorMsg == "Unauthenticated.") {
            this.errorMsg = "Session Expired";
            this.router.navigate(['/login']);
          }
          console.log(error.error.message);
          this.alertService.showAlert(error.error.message, true);
          // this.alertService.error(error);
          // this.loading = false;
        }

      });
    } else if (this.status == 2) {
      console.log('Drafted ready to publish')
      this.blogService.publishBlog(this.blog_id).pipe(first()).subscribe({
        next: (response) => {
          this.error = false;
          console.log(response);
          // this.status = 2;
          // this.blog_id = response['blog_id'];
          // console.log(JSON.parse(localStorage.getItem('user')?"true":"false"));
          // this.alertService.success('Registration successful', { keepAfterRouteChange: true });
          this.router.navigate(['/myblogs/']);
          this.alertService.showAlert('Blog Published Successfully', false);
        },
        error: error => {
          this.error = true;
          this.errorMsg = error.error.message;
          if (this.errorMsg == "Unauthenticated.") {
            this.errorMsg = "Session Expired";
            this.router.navigate(['/login']);
          }
          console.log(error.error.message);
          this.alertService.showAlert(error.error.message, true);
          // this.alertService.error(error);
          // this.loading = false;
        }

      });

    }
  }

}
