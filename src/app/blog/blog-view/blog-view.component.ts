import { DeleteModalComponent } from './../delete-modal/delete-modal.component';
import { apiUrl, mediaUrl } from 'src/globals';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { AuthService } from 'src/app/auth/auth.service';
import { Subject } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AlertService } from 'src/app/alert.service';
import { ActivatedRoute } from '@angular/router';
import { BlogService } from '../blog.service';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-blog-view',
  templateUrl: './blog-view.component.html',
  styleUrls: ['./blog-view.component.css']
})
export class BlogViewComponent implements OnInit {


  blog: any[];
  thumbnailUrl : string;
  blog_id:number;
  is_liked:boolean;
  constructor(private route: ActivatedRoute,private alertService :AlertService,private httpClient: HttpClient, private authService: AuthService,public matDialog: MatDialog, private blogService: BlogService) {
    this.thumbnailUrl = mediaUrl()+'blog_thumbnails/';
    this.route.params.subscribe(event => {

      this.blog_id = event.blog_id;
     });
  }

  ngOnInit(): void {
    this.getBlogData();
  }

  getBlogData():void{
    const user_id = JSON.parse(localStorage.getItem('user'))['user'].id;

    console.log(user_id)
    this.httpClient.post<string>(apiUrl() + 'blogs',
      {'user_id' : user_id , 'blog_id' : this.blog_id}, this.authService._header())
      .subscribe(data => {
        console.log(data);
        this.blog = data['blogs'];
        if(data['is_liked'].length > 0){
          this.is_liked = true;
        }else{
          this.is_liked = false;
        }
        // Calling the DT trigger to manually render the table

        // this.alertService.showAlert('Data Loaded' , false);
      });
  }

  onLike():void{
    const user_id = JSON.parse(localStorage.getItem('user'))['user'].id;
    this.blogService.storeLike( this.blog_id, user_id).pipe(first()).subscribe({
      next: (response) => {
        // this.error = false;
        console.log(response);
        this.getBlogData();


        // this.alertService.success('Registration successful', { keepAfterRouteChange: true });
        // this.router.navigate(['/myblogs']);
        // let currentUrl = this.router.url;
        // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        // this.router.onSameUrlNavigation = 'reload';
        // this.router.navigate([currentUrl]);


      },
      error: error => {
        // this.error = true;
        // this.errorMsg = error.error.message;

        // console.log(error.error.message);
        // this.alertService.error(error);
        // this.loading = false;
      }

    });

  }

}
