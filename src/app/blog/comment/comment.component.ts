import { BlogService } from './../blog.service';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/alert.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  blog_id: number;
  constructor(private alertService :AlertService,private route: ActivatedRoute, private router: Router, private blogService: BlogService) {
    this.route.params.subscribe(event => {

      this.blog_id = event.blog_id;
    });
  }
  @Input() comments: any;
  error: boolean;
  errorMsg: string;

  ngOnInit(): void {
  }

  onSubmit(commentForm): void {
    if (commentForm.invalid) {
      return;
    }
    // let blog_id =
    let user_id = JSON.parse(localStorage.getItem('user'))['user'].id
    this.blogService.storeComment(commentForm.value, this.blog_id, user_id).pipe(first()).subscribe({
      next: (response) => {
        this.error = false;
        console.log(response);


        // this.alertService.success('Registration successful', { keepAfterRouteChange: true });
        // this.router.navigate(['/myblogs']);
        let currentUrl = this.router.url;
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([currentUrl]);
        this.alertService.showAlert('Comment Successfull' , false);


      },
      error: error => {
        this.error = true;
        this.errorMsg = error.error.message;

        // console.log(error.error.message);
        // this.alertService.error(error);
        // this.loading = false;
      }

    });

  }

}
