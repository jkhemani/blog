import { UpdateCategoryModalComponent } from './../update-category-modal/update-category-modal.component';
import { apiUrl } from 'src/globals';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { AuthService } from 'src/app/auth/auth.service';
import { Subject } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DeleteModalComponent } from '../delete-modal/delete-modal.component';
import { AlertService } from 'src/app/alert.service';
// import { Person } from '../person';

class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
}

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  // constructor() { }

  dtOptions: DataTables.Settings = {};
  categories: any[];
  page_rows: number;
  dtTrigger: Subject<any> = new Subject<any>();
  constructor(private alertService :AlertService,public matDialog: MatDialog, private httpClient: HttpClient, private authService: AuthService) { }

  ngOnInit(): void {

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
    };
    this.getCategories();


  }
  setPageRows(value): void {
    this.page_rows = value
    this.getCategories();
  }
  getCategories(cat_id = 0, key = 'parent_id', search = "", url = apiUrl() + 'categories'): void {
    let data = {};
    console.log(key);
    console.log(cat_id);
    // if(key == 'id'){
    data[key] = cat_id;
    if (search != '') {
      data['search'] = search;
    }
    data['single_page'] = this.page_rows;
    // }
    this.httpClient.post<string>(url,
      data, this.authService._header())
      .subscribe(data => {
        console.log(data);
        if (data['categories']['data'].length > 0) {
          this.categories = data['categories'];
        }else{
          this.alertService.showAlert('No more Categories found' , true);
        }
        // Calling the DT trigger to manually render the table
        // this.dtTrigger.next();
      });
  }
  openDelModal(blog_id) {
    // console.log(blog_id);
    // let blog_id = param.target.id;
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "350px";
    dialogConfig.width = "600px";
    dialogConfig.data = { data: blog_id, del_type: 'category' };
    // https://material.angular.io/components/dialog/overview
    const modalDialog = this.matDialog.open(DeleteModalComponent, dialogConfig);
  }

  openUpdateModal(blog_id, category) {
    // console.log(blog_id);
    // let blog_id = param.target.id;
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "350px";
    dialogConfig.width = "600px";
    dialogConfig.data = { data: blog_id, category: category };
    // https://material.angular.io/components/dialog/overview
    const modalDialog = this.matDialog.open(UpdateCategoryModalComponent, dialogConfig);
  }

}
