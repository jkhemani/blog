import { EditBlogComponent } from './edit-blog/edit-blog.component';
import { MyBlogsComponent } from './my-blogs/my-blogs.component';
import { CategoriesComponent } from './categories/categories.component';
import { AuthGuard } from './../auth.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { RegisterComponent } from '../auth/register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateBlogComponent } from './create-blog/create-blog.component';
import { CategoriesCreateComponent } from './categories-create/categories-create.component';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogViewComponent } from './blog-view/blog-view.component';

const routes: Routes = [
  // localStorage.getItem('user') ?
  {
    path: '', canActivate: [AuthGuard], children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'blog/create', component: CreateBlogComponent },
      { path: 'categories', component: CategoriesComponent },
      { path: 'categories/create', component: CategoriesCreateComponent },
      { path: 'myblogs', component: MyBlogsComponent },
      { path: 'blogs', component: BlogsComponent },
      { path: 'blogs/:blog_id', component: BlogViewComponent },
      { path: 'editblogs/:blog_id', component: EditBlogComponent },
    ]
  }

  // :
  // { path: 'dashboard', redirectTo: '/login', pathMatch: 'full' }
  ,
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }
