import { MyBlogsComponent } from './../my-blogs/my-blogs.component';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Inject } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { BlogService } from '../blog.service';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/alert.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-category-modal',
  templateUrl: './update-category-modal.component.html',
  styleUrls: ['./update-category-modal.component.css']
})
export class UpdateCategoryModalComponent implements OnInit {

  constructor( private fb: FormBuilder,private alertService: AlertService, private router: Router, private blogService: BlogService, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<UpdateCategoryModalComponent>) {
    this.category = this.data.category;
  }
  category:string
  del_type: any;
  updateCat: FormGroup;
  ngOnInit(): void {
    console.log(this.data);
    this.updateCat = this.fb.group({
      category: [this.category, Validators.required],
      category_id: [this.data.data, Validators.required],

    }
    );
  }
  get updateCatControl() {
    return this.updateCat.controls;
  }
  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }

  @Input() blog_id: string;
  mySubscription: any;
  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  onSubmit():void{
    if (this.updateCat.invalid) {
      console.log(this.updateCat);
      this.validateAllFormFields(this.updateCat);
      return;
    }else{
      console.log(this.updateCat.value);
      this.blogService.updateCategories(this.updateCat.value).pipe(first()).subscribe({
        next: (response) => {
          this.closeModal();
          let currentUrl = this.router.url;
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate([currentUrl]);


          // console.log(JSON.stringify(response));
          this.alertService.showAlert('Category Updated Successfull' , false);

        },
        error: error => {
          this.closeModal();
          let currentUrl = this.router.url;
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate([currentUrl]);
          this.alertService.showAlert(error.error.message , true);

        }

      });
    }
  }
  // actionFunction() {
  //   let blog_id = this.data.data;

  //   if (this.del_type == 'category') {
  //     this.blogService.delCategory(blog_id).pipe(first()).subscribe({
  //       next: (response) => {
  //         // this.error = false;
  //         console.log(response);
  //         this.closeModal();
  //         let currentUrl = this.router.url;
  //         this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  //         this.router.onSameUrlNavigation = 'reload';
  //         this.router.navigate([currentUrl]);
  //         this.alertService.showAlert('Category Delete Successfull', false);
  //       },
  //       error: error => {

  //         console.log(error.error.message);
  //         this.alertService.showAlert(error.error.message, true);

  //       }

  //     });
  //   } else {
  //     this.blogService.inactiveBlog(blog_id).pipe(first()).subscribe({
  //       next: (response) => {
  //         // this.error = false;
  //         console.log(response);
  //         this.closeModal();
  //         let currentUrl = this.router.url;
  //         this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  //         this.router.onSameUrlNavigation = 'reload';
  //         this.router.navigate([currentUrl]);
  //         this.alertService.showAlert('Blog Inactive Successfull', false);
  //       },
  //       error: error => {

  //         console.log(error.error.message);
  //         this.alertService.showAlert(error.error.message, true);

  //       }

  //     });
  //   }


  // }

  closeModal() {
    this.dialogRef.close();
  }

}
