import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateBlogComponent } from './create-blog/create-blog.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CategoriesComponent } from './categories/categories.component';
import { DataTablesModule } from "angular-datatables";
import { CategoriesCreateComponent } from './categories-create/categories-create.component';
import { MyBlogsComponent } from './my-blogs/my-blogs.component';
import { TruncatePipe } from '../tuncatehtml.pipe';
import {  RxReactiveFormsModule } from "@rxweb/reactive-form-validators";
import { DeleteModalComponent } from './delete-modal/delete-modal.component'
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { AlertComponent } from './alert/alert.component';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogViewComponent } from './blog-view/blog-view.component';
import { CommentComponent } from './comment/comment.component';
import { DateAgoPipe } from '../pipes/date-ago.pipe';
import { EditBlogComponent } from './edit-blog/edit-blog.component';
import { UpdateCategoryModalComponent } from './update-category-modal/update-category-modal.component';
import { NgxBootstrapMultiselectModule } from 'ngx-bootstrap-multiselect';
import { ToastrModule } from 'ngx-toastr';
import {ScrollingModule} from '@angular/cdk/scrolling';
import { TableComponent } from '../layout/table/table.component';
// import {TextFieldModule } from '@angular/cdk/text-field';
// import {MatFormFieldModule} from '@angular/material/form-field';
@NgModule({
  declarations: [TableComponent,DateAgoPipe,DashboardComponent, CreateBlogComponent, CategoriesComponent, CategoriesCreateComponent, MyBlogsComponent,TruncatePipe, DeleteModalComponent, AlertComponent, BlogsComponent, BlogViewComponent, CommentComponent, EditBlogComponent, UpdateCategoryModalComponent,],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    NgxBootstrapMultiselectModule,
    DataTablesModule,
    MatButtonModule,
    MatDialogModule,
    CKEditorModule,
    BlogRoutingModule,
    ScrollingModule,
    ToastrModule.forRoot(),
    // TextFieldModule,
    // MatFormFieldModule,



  ],
  entryComponents: [DeleteModalComponent]
  // declarations: [ ],
})
export class BlogModule { }
