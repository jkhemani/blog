import { DeleteModalComponent } from './../delete-modal/delete-modal.component';
import { apiUrl } from 'src/globals';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { AuthService } from 'src/app/auth/auth.service';
import { Subject } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AlertService } from 'src/app/alert.service';

@Component({
  selector: 'app-my-blogs',
  templateUrl: './my-blogs.component.html',
  styleUrls: ['./my-blogs.component.css']
})
export class MyBlogsComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  blogs: any[];
  dtTrigger: Subject<any> = new Subject<any>();
  page_rows: number;
  columns = ['ID' ,'', 'Title' ,'Status' , 'Body'];
  params = {
    'data_access_key' : 'blogs',
    'user_id':JSON.parse(localStorage.getItem('user'))['user'].id
  }
  constructor(private alertService: AlertService, private httpClient: HttpClient, private authService: AuthService, public matDialog: MatDialog) { }
  setPageRows(value): void {
    this.page_rows = value
    this.getMyBlogs();
  }
  openDelModal(blog_id) {
    // console.log(blog_id);
    // let blog_id = param.target.id;
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "350px";
    dialogConfig.width = "600px";
    dialogConfig.data = { data: blog_id };
    // https://material.angular.io/components/dialog/overview
    const modalDialog = this.matDialog.open(DeleteModalComponent, dialogConfig);
  }
  ngOnInit(): void {
    this.getMyBlogs();
  }

  getMyBlogs(search = "" ,url = apiUrl() + 'blogs'): void {

    const user_id = JSON.parse(localStorage.getItem('user'))['user'].id;

    // this.dtOptions = {
    //   pagingType: 'full_numbers',
    //   pageLength: 10,
    // };
    this.httpClient.post<string>(url,
      { 'user_id': user_id , 'single_page' :this.page_rows ,'search':search}, this.authService._header())
      .subscribe(data => {
        console.log(data);
        this.blogs = data['blogs'];
        // Calling the DT trigger to manually render the table
        // this.dtTrigger.next();
        // this.alertService.showAlert('Data Loaded' , false);
      });
  }


}
