
import { DeleteModalComponent } from './../delete-modal/delete-modal.component';
import { apiUrl, mediaUrl } from 'src/globals';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { AuthService } from 'src/app/auth/auth.service';
import { Subject } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AlertService } from 'src/app/alert.service';
import {ChangeDetectionStrategy} from '@angular/core';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css'],
  // changeDetection: ChangeDetectionStrategy.Default,
})
export class BlogsComponent implements OnInit {
  items = Array.from({length: 100000}).map((_, i) => `Item #${i}`);
  blogs =[];
  blogs_length:number;
  thumbnailUrl : string;

  constructor(private alertService :AlertService,private httpClient: HttpClient, private authService: AuthService,public matDialog: MatDialog) {
    this.thumbnailUrl = mediaUrl()+'blog_thumbnails/';
    // this.getBlogs();
  }


  ngOnInit(): void {
    this.getBlogs();


  }

  getBlogs():void{
    const user_id = JSON.parse(localStorage.getItem('user'))['user'].id;
    console.log(user_id)
    this.httpClient.post<string>(apiUrl() + 'blogs',
      {'user_id' : user_id , 'all' :'all'}, this.authService._header())
      .subscribe(data => {
        console.log(data);
        this.blogs = data['blogs'];
        this.blogs_length = this.blogs.length
        // Calling the DT trigger to manually render the table

        // this.alertService.showAlert('Data Loaded' , false);
      });
  }

}
