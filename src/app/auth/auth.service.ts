import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { apiUrl } from 'src/globals';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiURL: string = apiUrl();
  headers;

  public _header(){
    return this.headers;
  }


  constructor(private httpClient: HttpClient) {
    this.headers = {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${JSON.parse(localStorage.getItem('user'))['token']}`)
    }
    // console.log(this.apiURL);
  }
  public login(loginCred) {
    return this.httpClient.post<string>(`${this.apiURL}login`, loginCred)
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('user', JSON.stringify(user));
        this.headers = {
          headers: new HttpHeaders()
            .set('Authorization', `Bearer ${JSON.parse(localStorage.getItem('user'))['token']}`)
        }

        // this.userSubject.next(user);
        return user;


      }));
  }
  public register(registerCred) {
    return this.httpClient.post<string>(`${this.apiURL}register`, registerCred)
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('user', JSON.stringify(user));
        this.headers = {
          headers: new HttpHeaders()
            .set('Authorization', `Bearer ${JSON.parse(localStorage.getItem('user'))['token']}`)
        }

        // this.userSubject.next(user);
        return user;


      }));
  }
  public logout() {
    // localStorage.removeItem('user');
    this.headers = {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${JSON.parse(localStorage.getItem('user'))['token']}`)
    }
    console.log(JSON.parse(localStorage.getItem('user'))['token']);
    return this.httpClient.post<string>(`${this.apiURL}logout`, {}, this.headers)
      .pipe(map(user => {

        // remove user from local storage and set current user to null
        localStorage.removeItem('user');
      }));
  }
  public resend_email(token) {
    // localStorage.removeItem('user');
    this.headers = {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${token}`)
    }
    // console.log(JSON.parse(localStorage.getItem('user'))['token']);
    return this.httpClient.post<string>(`${this.apiURL}email/resend`, {}, this.headers)
      .pipe(map(response => {
        return response;
        // remove user from local storage and set current user to null
        // localStorage.removeItem('user');
      }));
  }


}
