import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  error: boolean;
  errorMsg: string;
  token: string;
  resendEmail:boolean = false;


  constructor(private router: Router, private authService: AuthService) {
    // if (localStorage.getItem('user')) {
    //   this.router.navigate(['/dashboard']);
    // }
  }

  ngOnInit(): void {
  }

  resendEmailNow():void{
    console.log('Resend email')
    this.authService.resend_email(this.token).pipe(first()).subscribe({
      next: (response) => {
        this.resendEmail =false;
        // console.log(response);
        // this.error = false;
        // console.log(JSON.parse(localStorage.getItem('user')?"true":"false"));
        // this.alertService.success('Registration successful', { keepAfterRouteChange: true });
        this.error = true;
        this.errorMsg = response['message'];
        // this.router.navigate(['/login']);
      },
      error: error => {
        this.error = true;
        this.errorMsg = error.error.message;
        console.log(error.error.message);

        // this.alertService.error(error);
        // this.loading = false;
      }

    });
  }


  onSubmit(loginform): void {
    if (loginform.invalid) {
      return;
    }
    // console.log(loginform.value);
    this.authService.login(loginform.value).pipe(first()).subscribe({
      next: (response) => {
        this.error = false;
        console.log(response);

          console.log(JSON.parse(localStorage.getItem('user') ? "true" : "false"));
          // this.alertService.success('Registration successful', { keepAfterRouteChange: true });
          this.router.navigate(['/myblogs']);


      },
      error: error => {
        this.error = true;
        this.errorMsg = error.error.message;
        if (this.errorMsg == "Email is not verifed") {
          this.resendEmail =true;
          this.token = error.error.token;
        } else {
          this.resendEmail =false;
        }
        // console.log(error.error.message);
        // this.alertService.error(error);
        // this.loading = false;
      }

    });
  }

}
