import { AuthGuard } from './../auth.guard';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SecureInnerPagesGuard } from '../secure-inner-pages.guard';


const routes: Routes = [
  // { path: 'login', component: LoginComponent },
  // localStorage.getItem('user') ?
  // { path: 'register', redirectTo: '/dashboard', pathMatch: 'full' } :
  {
    path: '', canActivate: [SecureInnerPagesGuard], children: [
      { path: 'register', component: RegisterComponent, canActivate: [SecureInnerPagesGuard] },
      { path: 'login', component: LoginComponent, canActivate: [SecureInnerPagesGuard] },
    ]
  }
  // localStorage.getItem('user') ?
  // { path: 'login', redirectTo: '/dashboard', pathMatch: 'full' } :

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
