import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  error:boolean;
  errorMsg:string;

  constructor(private router: Router,private authService: AuthService) {
    // if (localStorage.getItem('user')) {
    //   this.router.navigate(['/dashboard']);
    // }
   }

  ngOnInit(): void {
  }
  onSubmit(registerForm): void {
    if (registerForm.invalid) {
      return;
    }
    console.log(registerForm.value);
    this.authService.register(registerForm.value).pipe(first()).subscribe({
      next: (response) => {
        this.error = false;
        // console.log(response);

        console.log(JSON.parse(localStorage.getItem('user')?"true":"false"));
        // this.alertService.success('Registration successful', { keepAfterRouteChange: true });
        this.router.navigate(['/myblogs']);
      },
      error: error => {
        this.error = true;
        this.errorMsg = error.error.message;
        if(this.errorMsg == "Unauthenticated."){
          this.errorMsg = "Session Expired";
          this.router.navigate(['/login']);
        }
        // console.log(error.error);
        // this.alertService.error(error);
        // this.loading = false;
      }

    });
  }

}
