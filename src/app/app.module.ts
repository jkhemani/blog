import { AuthModule } from './auth/auth.module';
import { BlogModule } from './blog/blog.module';
import { CommonModule } from '@angular/common';
// import { AuthRoutingModule } from './auth/auth-routing.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DateAgoPipe } from './pipes/date-ago.pipe';
// import { TableComponent } from './layout/table/table.component';



@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    // TableComponent,
    // DateAgoPipe,

  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    // CKEditorModule,
    HttpClientModule,
    AuthModule,
    BlogModule,
    AppRoutingModule,
    BrowserAnimationsModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
