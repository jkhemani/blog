
import { Component, Input, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { AuthService } from 'src/app/auth/auth.service';
import { Subject } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AlertService } from 'src/app/alert.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  constructor(private alertService: AlertService, private httpClient: HttpClient, private authService: AuthService, public matDialog: MatDialog) { }
  @Input() data : any;
  @Input() columns : any;
  @Input() action : any;
  @Input() params : any;
  page_rows:number;
  // search:any;
  // url:any;

  ngOnInit(): void {
  }

  setPageRows(value): void {
    this.page_rows = value
    this.getData();
  }

  getData(search = "" ,url = "" ):void{

    let formPost = {};

    formPost['single_page'] = this.page_rows;
    formPost['search'] = search;
    for (let key in this.params) {
      let value = this.params[key];
      formPost[key] = value;
    }


    this.httpClient.post<string>(url,
      formPost, this.authService._header())
      .subscribe(data => {
        console.log(data);
        this.data = data[this.params['data_access_key']];
      });

  }



}
